# Dotfiles
---

### How to install

```bash
$ make install-[name package]
```

Hey,

This are my public dotfiles. They might not work for you, but feel free to steal from them.
