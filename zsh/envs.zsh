export ANACONDA=$HOME/anaconda3
export PY_ENV=$ANACONDA/bin
export PYTHONPATH=$PY_ENV/python3
export M2_HOME=/usr/local/opt/apache-maven-3.6.1
export M2=$M2_HOME/bin
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-12.0.1.jdk/Contents/Home
export GOROOT=$HOME/.local/go
export GO=$GOROOT/bin
export GOPATH=$HOME/workspace/go
export GOBIN=$GOPATH/bin
export PATH=$M2:$GO:$PY_ENV:$GOPATH:$GOBIN:$PATH:/usr/local/bin
export PYTHONDONTWRITEBYTECODE=1
export VIRTUAL_ENV_DISABLE_PROMPT=1
export LOCATION=`location`
if [[ -z $LOCATION ]]; then
    LOCATION="everywhere"
fi
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
