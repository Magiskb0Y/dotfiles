" Vim color file - based on desert
" Maintainer:	Hans Fugal <hans@fugal.net>
" Last Change:	$Date: 2004/06/13 19:30:30 $
" Last Change:	$Date: 2004/06/13 19:30:30 $
" URL:		http://hans.fugal.net/vim/colors/desert.vim
" Version:	$Id: desert.vim,v 1.1 2004/06/13 19:30:30 vimboss Exp $

" cool help screens
" :he group-name
" :he highlight-groups
" :he cterm-colors

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi clear
    if exists("syntax_on")
	syntax reset
    endif
endif
let g:colors_name="desert"

" color terminal definitions
hi SpecialKey	cterm=NONE ctermfg=darkgreen
hi NonText	    cterm=bold ctermfg=darkblue
hi Directory	cterm=NONE ctermfg=darkcyan
hi ErrorMsg	    cterm=bold ctermfg=7 ctermbg=1
hi IncSearch	cterm=NONE ctermfg=yellow ctermbg=green
hi Search	    cterm=bold ctermfg=7 ctermbg=35
hi MoreMsg	    cterm=NONE ctermfg=darkgreen
hi ModeMsg	    cterm=NONE ctermfg=brown
hi LineNr	    cterm=NONE ctermfg=3
hi Question	    cterm=NONE ctermfg=green
hi StatusLine	cterm=bold,reverse ctermfg=lightgray
hi StatusLineNC cterm=reverse
hi ColorColumn  cterm=NONE ctermbg=234
hi SignColumn   cterm=NONE ctermbg=NONE
hi VertSplit	cterm=reverse
hi Title	    cterm=NONE ctermfg=5
hi Visual	    cterm=reverse
hi VisualNOS	cterm=bold,underline
hi WarningMsg	cterm=NONE ctermfg=1
hi WildMenu	    cterm=NONE ctermfg=0 ctermbg=3
hi Folded	    cterm=NONE ctermfg=darkgrey ctermbg=NONE
hi FoldColumn	cterm=NONE ctermfg=darkgrey ctermbg=NONE
hi DiffAdd	    cterm=NONE ctermbg=4
hi DiffChange	cterm=NONE ctermbg=5
hi DiffDelete	cterm=bold ctermfg=4 ctermbg=6
hi DiffText	    cterm=bold ctermbg=1
hi Comment	    cterm=NONE ctermfg=240
hi Constant	    cterm=NONE ctermfg=brown
hi Special	    cterm=NONE ctermfg=5
hi Identifier	cterm=NONE ctermfg=6
hi Statement	cterm=NONE ctermfg=3
hi PreProc	    cterm=NONE ctermfg=5
hi Type		    cterm=NONE ctermfg=2
hi Underlined	cterm=underline ctermfg=5
hi Ignore	    cterm=bold ctermfg=7
hi Ignore	    cterm=NONE ctermfg=darkgrey
hi Error	    cterm=bold ctermfg=7 ctermbg=1
hi CursorLine   cterm=bold ctermbg=234
hi String       cterm=NONE ctermfg=227
hi Function     cterm=NONE ctermfg=38
hi ALEWarning   cterm=bold ctermbg=234
hi Pmenu        cterm=NONE ctermbg=234 ctermfg=247
hi PmenuSel     cterm=NONE ctermbg=240 ctermfg=250
